

var saugotiBtn = document.getElementById("saugoti");

 // gauname visa html elemento objekta
var laikasInput = document.getElementById("laikas");
var dataInput = document.getElementById("data");

var numeriaiInput = document.getElementById("numeriai");
var atstumasInput = document.getElementById("atstumas");

var masinos = [];

var trintiButton = document.getElementById("trinti");

// document.getElementById("laikas").value.value;

saugotiBtn.addEventListener("click", function() {
    var masina = [];

    // issaugau duomenis is html inputu i savo darbini masinos masyva
    masina[0] = dataInput.value;
    masina[1] = numeriaiInput.value;
    masina[2] = parseInt(laikasInput.value);
    masina[3] = parseInt(atstumasInput.value);
    // suskaiciuoju greiti
    masina[4] = skaiciuotiGreiti(atstumasInput.value, laikasInput.value);
    masina[4] = masina[4];

    // i masinu masyva irasau konkrecios masinos duomenis
    masinos.push(masina);

    // atspausdinu konkrecios masinos duomenis
    spausdintiDuomenis(masina);

    console.log("masinu masyvas: ");
    console.log(masinos);
    resetForm();

    document.querySelector("#vidurkis span").innerText = vidutinisGreitis();

});

trintiButton.addEventListener("click", function() {
    console.log("trinu duomenis");
    document.getElementById("duomenys").innerHTML = "";
    masinos = [];
});

function vidutinisGreitis() {
    var greiciuSuma = 0;
    var rezultatas;
    for(var i = 0; i < masinos.length; i++) {
        greiciuSuma += masinos[i][4];
    }
    rezultatas = greiciuSuma / masinos.length;

    return parseInt(rezultatas);
}

function resetForm() {
    dataInput.value = "";
    numeriaiInput.value = "";
    laikasInput.value = "";
    atstumasInput.value = "";
}


function spausdintiDuomenis(masina) {
    var row = document.createElement("tr");

    for(var i = 0; i < masina.length; i++) {
        var col = document.createElement("td");
        col.innerText = masina[i];

        row.appendChild(col);
    }

    document.getElementById("duomenys").appendChild(row);
}




function skaiciuotiGreiti(atstumas, laikas) {
    var rezultatas = 0;

    rezultatas = (atstumas / laikas) / 3600 * 1000;

    return rezultatas;
}